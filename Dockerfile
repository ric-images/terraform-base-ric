FROM ubuntu:bionic-20200219

# Common variables | versions from binaries 
ARG TERRAFORM_VERSION=0.13.2
ARG PYTHON_MAJOR_VERSION=3.7
ARG PACKER_VERSION=1.5.4

# metadata of image
LABEL maintainer="Ricardocg <ricguerrero94@gmail.com>"
LABEL terraform_version=${TERRAFORM_VERSION}
LABEL python_major=${PYTHON_MAJOR_VERSION}
LABEL description="Base image with terraform and utilities dof different Cloud Providers"

ENV DEBIAN_FRONTEND=noninteractive
ENV TERRAFORM_VERSION=${TERRAFORM_VERSION}
ENV PACKER_VERSION=${PACKER_VERSION}

COPY Gemfile . 

RUN apt-get update \
    && apt-get install -y ansible curl python3 python3-pip python3-boto unzip git ruby-full wget build-essential make gnupg \
    build-essential make gnupg zlib1g-dev libssl-dev libreadline-dev libgdbm-dev openssl \
    && curl -LO https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip \
    && curl -LO https://releases.hashicorp.com/packer/${PACKER_VERSION}/packer_${PACKER_VERSION}_linux_amd64.zip \
    && unzip '*.zip' -d /usr/local/bin \
    && rm *.zip

#Install bundler and Gems for kitchen test
RUN gem install bundler \
    && bundle install \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

#Install OPA
RUN curl -L -o opa https://openpolicyagent.org/downloads/latest/opa_linux_amd64 \
    && chmod 755 ./opa \
    && mv opa /usr/local/bin/ 


CMD    ["/bin/bash"]